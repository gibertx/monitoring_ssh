import datetime
import os
import smtplib
import time

import paramiko

debag_mail = True

# environment for mail
host_mail = os.getenv('HOST_MAIL')
port_mail = int(os.getenv('PORT_MAIL'))
fromaddr = 'AUTO alarm {}'.format(os.getenv('FROM_ADDR'))
password_fromaddr = os.getenv('PASSWORD_FROM_ADDR')

# environment for server monitoring
ip_server_monitoring = os.getenv('IP_SERVER_MONITORING')
user_server_monitoring = os.getenv('USER_SERVER_MONITORING')
password_server_monitoring = os.getenv('PASSWORD_SERVER_MONITORING')

MAIL_ALARM = 'test_mail_alarm@test.ru'


def check_status_for_ssh(hostname,
                         username,
                         password,
                         command,
                         right_status,
                         mail_alarm,
                         server,
                         log=None,
                         len_log=10,
                         grep='.'
                         ):
    """ Check server status for ssh  client

    Parameters:
    hostname (str): server hostname
    username (str): server username
    password (str): password for username
    command (str): command for checking status
    right_status (str): expected status
    mail_alarm (str): email to alert
    server (obj): email SMTP server
    log (str): absolute path to the log
    len_log (int): length log
    grep (str): filter to grep


    Returns list:
    title_mail (str): server warning
    success_send_mail (bool): success of sending the letter

   """
    try:
        # try connect for ssh
        cl = paramiko.SSHClient()
        cl.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        cl.connect(
                  hostname=hostname,
                  username=username,
                  password=password,
                  look_for_keys=False,
                  allow_agent=False
                  )
        # send command and read stdout and stderr
        _, stdout, stderr = cl.exec_command(command)
        data_out = stdout.read().decode("utf-8")
        if right_status in data_out.split():
            return []
        else:
            title_mail = 'Warning for host {}'.format(hostname)
            data_mail = stderr.read().decode("utf-8")
            if log:
                _, stdout, stderr = cl.exec_command(
                    'tail -n {} {} | grep \'{}\''.format(len_log, log, grep))
                data_mail += stdout.read().decode("utf-8")
            success_send_mail = send_mail(mail_alarm, title_mail,
                                          data_mail, server)
            return[title_mail, success_send_mail]
    except Exception as identifier:
        print('Exception check status for ssh: ', identifier)
    finally:
        cl.close()


def send_mail(adress_list, title, data, server):
    """ Send mail

    Parameters:
    adress_list (list): adress for notifications
    title (str): title message
    data (str): data message
    server (obj): obj SMTP class

    Returns:
    success_send_mail (bool): success of sending the letter

   """
    if debag_mail:
        print('Send mail ', adress_list, title, data)
        return True
    else:
        msg = 'From: {}\nTo: {}\nSubject: {}\n\n{}'.format(fromaddr,
                                                           adress_list,
                                                           title, data)
        try:
            if isinstance(adress_list, list):
                for adress in adress_list:
                    server.sendmail(fromaddr, adress, msg)
            else:
                server.sendmail(fromaddr, adress_list, msg)
            return True
        except Exception as identifier:
            print('Exception send mail: ', identifier)
            return False


def monitor():
    """ System monitoring at a certain time interval"""
    server = smtplib.SMTP(host_mail, port_mail, timeout=10)
    # server.set_debuglevel(1)
    try:
        if not debag_mail:
            server.starttls()
            server.login(fromaddr, password_fromaddr)
        # creating commands for checking
        # read docstring check_status_for_ssh
        # next str for example
        # check_status_for_ssh(ip_server_monitoring, user_server_monitoring,
        #                      password_server_monitoring, 'dspmq', 'Running',
        #                      MAIL_ALARM, server, 'log.txt')

        num_check = 0
        list_err = []
        # monitoring every 10 min, and send mail for 1 hour
        while True:
            starttime = time.time()
            now = datetime.datetime.now()
            if (num_check == 6) or ((num_check > 1) and (now.minute < 10)):
                if not debag_mail:
                    send_mail(MAIL_ALARM,
                              'Error notification for the last hour\
                               {}'.format(len(list_err)),
                              list_err, server)
                else:
                    print(now, MAIL_ALARM,
                          'Error notification for the last hour\
                           {}'.format(len(list_err)),
                          list_err, server)
                num_check = 0
                list_err = []

            exe_stat = check_status_for_ssh(ip_server_monitoring,
                                            user_server_monitoring,
                                            password_server_monitoring,
                                            'dspmq', 'Running',
                                            MAIL_ALARM, server, 'log.txt')
            num_check += 1
            if exe_stat:
                list_err.append('Problem {}.Send mail {}'.format(exe_stat[0],
                                                                 exe_stat[1]))
            time.sleep(600.0 - ((time.time() - starttime) % 600.0))

    except Exception as identifier:
            print('Exception monitor: ', identifier)
    finally:
        if not debag_mail:
            server.quit()

if __name__ == "__main__":
    monitor()
